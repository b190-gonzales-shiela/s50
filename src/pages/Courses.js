import { Fragment, useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
/*import coursesData from '../data/coursesData';
*/

/*
Miniactivity:
	remove the CourseCard component from Home to Courses.js page.

	replace the h1 element in the return statement with the "sample course" card from the Home Page

	5 mins = 6:15 pm (feel free to send the screenshot of the code snippets in our google chat)
*/

/*
	the course in our CourseCard component can be sent as a prop
		prop - is a shorthand of "property" since the components are considered as object in ReactJs
	the curly braces {} are used for props to signify that we are providing information using JS expressions rather than hard coded values which use double quotes ""

*/
export default function Courses(){
	// console.log(coursesData);

	const [ courses, setCourses] = useState([]);

	useEffect(()=>{

		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data);


			setCourses(data.map(course=>{
				return(

					<CourseCard key={course._id} courseProp={course} />
					);
			}));
		})
	},[])

	/*const courses = coursesData.map(course =>{
		return(
			<CourseCard key={course.id} courseProp={course} />
		)
	})*/

	return(
		<Fragment>
			{courses}
		</Fragment>
	)
}