import {Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
						<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
						Donec bibendum rhoncus ante, a rutrum urna auctor id. Suspendisse commodo magna sit amet sodales pharetra. Sed libero odio, viverra vitae sodales eget, gravida gravida elit. Morbi porta ullamcorper lectus, in vehicula dolor ullamcorper vitae. Aliquam gravida blandit nisl ut rutrum. In in felis in nibh viverra fermentum. Cras et luctus neque, quis sodales massa. Fusce pulvinar mi a tortor cursus, et sollicitudin orci malesuada. Curabitur quam libero, bibendum at nisi eget, ultricies viverra nisl. Nunc eros libero, consequat vitae rhoncus ut, consectetur quis leo. Cras mollis nibh eu enim sodales, non imperdiet diam pulvinar. Nam vitae ultricies urna, et tempus libero. Praesent quis rutrum dolor. Fusce gravida, sem at cursus ornare, odio massa lobortis odio, eu posuere velit est id leo. Donec in magna eu est dapibus commodo ac eu mauris.

						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
				<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
						<h2>Study Now Pay Later</h2>
						</Card.Title>
						<Card.Text>
						Donec bibendum rhoncus ante, a rutrum urna auctor id. Suspendisse commodo magna sit amet sodales pharetra. Sed libero odio, viverra vitae sodales eget, gravida gravida elit. Morbi porta ullamcorper lectus, in vehicula dolor ullamcorper vitae. Aliquam gravida blandit nisl ut rutrum. In in felis in nibh viverra fermentum. Cras et luctus neque, quis sodales massa. Fusce pulvinar mi a tortor cursus, et sollicitudin orci malesuada. Curabitur quam libero, bibendum at nisi eget, ultricies viverra nisl. Nunc eros libero, consequat vitae rhoncus ut, consectetur quis leo. Cras mollis nibh eu enim sodales, non imperdiet diam pulvinar. Nam vitae ultricies urna, et tempus libero. Praesent quis rutrum dolor. Fusce gravida, sem at cursus ornare, odio massa lobortis odio, eu posuere velit est id leo. Donec in magna eu est dapibus commodo ac eu mauris.

						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
						<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
						Donec bibendum rhoncus ante, a rutrum urna auctor id. Suspendisse commodo magna sit amet sodales pharetra. Sed libero odio, viverra vitae sodales eget, gravida gravida elit. Morbi porta ullamcorper lectus, in vehicula dolor ullamcorper vitae. Aliquam gravida blandit nisl ut rutrum. In in felis in nibh viverra fermentum. Cras et luctus neque, quis sodales massa. Fusce pulvinar mi a tortor cursus, et sollicitudin orci malesuada. Curabitur quam libero, bibendum at nisi eget, ultricies viverra nisl. Nunc eros libero, consequat vitae rhoncus ut, consectetur quis leo. Cras mollis nibh eu enim sodales, non imperdiet diam pulvinar. Nam vitae ultricies urna, et tempus libero. Praesent quis rutrum dolor. Fusce gravida, sem at cursus ornare, odio massa lobortis odio, eu posuere velit est id leo. Donec in magna eu est dapibus commodo ac eu mauris.

						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)
}