import { Card, Button } from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
// we can try to destructure the props object recieved from the parent component(Courses.js) so that the main object from the mock database will be accessed
export default function CourseCard({courseProp}) {

    console.log(courseProp);    
    console.log(typeof courseProp);

    // destructure the courseProp object for more code readability and ease of coding for the part of the dev (doesn't have to access multiple objects before accessing the needed properties)
    const { name, description, price, _id } = courseProp || {};

 /*   const [count, setCount]= useState(0);
    const [seats, setSeats]= useState(10);
    const [isOpen, setIsOpen]= useState(true);

    function enroll(){
        setCount(count + 1); //count=+1
        console.log(`Enrollees: ${count}`);
        setSeats(seats -1);
        console.log(`Seats: ${seats}`)
    }




    useEffect(() =>{

        if(seats===0){
            setIsOpen(false);
        }
    },[seats]);

    /*two way binding
        refers to the domino effect of changign the state going into the change of the display in the UI front end

    */

// */
   

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>

                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>

                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>

               
                <Link className='btn btn-primary' to ={`/courses/${_id}`}> Details </Link>
              
            
            </Card.Body>
        </Card>
    );
};
