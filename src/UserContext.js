import React from 'react';

// Creates UserContext object

/*
	createContext method allows us to store a differenmt data inside an object, in this case called UserContext. This set of information can be shared to other components within the app

*/
const UserContext= React.createContext();
/*
	Provider component allows other components to consumme/ use the context object and support necessary information needed to the context object

*/

// export without default needs destructuring before being to be imported by other components

export const UserProvider= UserContext.Provider;

export default UserContext;
